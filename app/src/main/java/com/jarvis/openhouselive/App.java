package com.jarvis.openhouselive;

import android.app.Application;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppManager.getInstance().setContext(this);
    }
}
