package com.jarvis.openhouselive.rest;

import com.jarvis.openhouselive.model.response.ListingResponse;
import com.jarvis.openhouselive.model.response.LocalityResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public interface RestService {

    @GET("/api.php")
    void getListings(@Query("l") String locality, Callback<ListingResponse> callback);

    @GET("/test1.php")
    void getLocalities(@Query("city") String city, Callback<LocalityResponse> callback);
}
