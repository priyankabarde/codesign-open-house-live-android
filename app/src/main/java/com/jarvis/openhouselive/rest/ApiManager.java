package com.jarvis.openhouselive.rest;

import com.jarvis.openhouselive.model.response.ListingResponse;
import com.jarvis.openhouselive.model.response.LocalityResponse;

import java.lang.ref.WeakReference;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class ApiManager extends RestBase {

    private static ApiManager ourInstance = new ApiManager();

    private RestService restService;

    public static ApiManager getInstance() {
        return ourInstance;
    }

    private ApiManager() {
        restService = getRestService();
    }

    public void getListings(final WeakReference<Requester> weakReference, String city, String locality, String type, String minPrice, String maxPrice) {
        Callback<ListingResponse> callback = new Callback<ListingResponse>() {
            @Override
            public void success(ListingResponse listingResponse, Response response) {
                Requester requester = weakReference.get();
                if (requester != null) {
                    requester.success(listingResponse);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Requester requester = weakReference.get();
                if (requester != null) {
                    requester.failure();
                }
            }
        };

        restService.getListings(locality, callback);
    }

    public void getLocalities(final WeakReference<Requester> weakReference, String city) {
        Callback<LocalityResponse> callback = new Callback<LocalityResponse>() {
            @Override
            public void success(LocalityResponse LocalityResponse, Response response) {
                Requester requester = weakReference.get();
                if (requester != null) {
                    requester.success(response);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Requester requester = weakReference.get();
                if (requester != null) {
                    requester.failure();
                }
            }
        };

        restService.getLocalities(city, callback);
    }
}
