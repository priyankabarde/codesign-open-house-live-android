package com.jarvis.openhouselive.util;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public interface Constants {

    String TAG_FRAGMENT_ABOUT = "tag_fragment_about";
    String TAG_FRAGMENT_MAIN = "tag_fragment_main";
    String TAG_FRAGMENT_PROFILE = "tag_fragment_profile";

    String EXTRA_KEY_DESCRIPTION = "extra_key_description";
    String EXTRA_KEY_IMAGE = "extra_key_image";
    String EXTRA_KEY_LABEL = "extra_key_label";
    String EXTRA_KEY_LISTING = "extra_key_listing";
    String EXTRA_KEY_MARKER = "extra_key_marker";
}
