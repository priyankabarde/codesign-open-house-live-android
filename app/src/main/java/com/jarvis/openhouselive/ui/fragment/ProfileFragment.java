package com.jarvis.openhouselive.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.util.Constants;

/**
 * Created by meeth.dinesh on 01/11/15.
 */
public class ProfileFragment extends Fragment {

    public static ProfileFragment newInstance(Bundle bundle) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String getMyTag() {
        return Constants.TAG_FRAGMENT_PROFILE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
}
