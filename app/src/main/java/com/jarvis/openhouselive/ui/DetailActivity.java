package com.jarvis.openhouselive.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.jarvis.openhouselive.AppManager;
import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.model.common.Listing;
import com.jarvis.openhouselive.ui.custom.CustomTextView;
import com.jarvis.openhouselive.util.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailActivity extends AppCompatActivity {

    @Bind(R.id.detail_listing_address)
    CustomTextView address;
    @Bind(R.id.detail_listing_price)
    CustomTextView price;
    @Bind(R.id.detail_listing_locality)
    CustomTextView locality;
    @Bind(R.id.detail_listing_type)
    CustomTextView type;
    @Bind(R.id.detail_listing_area)
    CustomTextView area;
    @Bind(R.id.detail_listing_amenities)
    CustomTextView amenities;

    private Listing listing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String listingString = bundle.getString(Constants.EXTRA_KEY_LISTING);
            listing = AppManager.getObjectFromJson(listingString, Listing.class);
        }

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(listing.getName());

        loadListingDetails();
    }

    private void loadListingDetails() {
        ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        Picasso.with(this).load(listing.getImageUrl()).into(imageView);

        address.setText(listing.getAddress());
        area.setText(listing.getArea());
        locality.setText(listing.getLocality());
        price.setText("\u20B9 " + listing.getPrice());
        type.setText(listing.getType());

        String[] list = listing.getAmenities();
        String value = "";
        int index = 0;
        for (String item:list) {
            value = value + item;
            if (index++ < list.length - 1) {
                value = value + " • ";
            }
        }
        amenities.setText(value);
    }

    @OnClick({R.id.fab_navigate})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_navigate : {
                launchNav("12.9347893","77.6101299");
                break;
            }
        }
    }

    private void launchNav(String lat, String lng) {
        String url = "google.navigation:q=" + lat + "," + lng + "&mode=d";
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }
}
