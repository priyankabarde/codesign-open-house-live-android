package com.jarvis.openhouselive.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.ui.custom.CustomButton;
import com.jarvis.openhouselive.ui.custom.CustomTextView;
import com.jarvis.openhouselive.util.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class ScreenFragment extends Fragment {

    @Bind(R.id.view_pager_description)
    CustomTextView descriptionTextView;
    @Bind(R.id.view_pager_label)
    CustomTextView labelTextView;
    @Bind(R.id.view_pager_image)
    ImageView imageView;

    private String description, label;
    private int resourceId = -1;

    public static ScreenFragment newInstance(Bundle bundle) {
        ScreenFragment fragment = new ScreenFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (null != bundle) {
            description = bundle.getString(Constants.EXTRA_KEY_DESCRIPTION, description);
            label = bundle.getString(Constants.EXTRA_KEY_LABEL, label);
            resourceId = bundle.getInt(Constants.EXTRA_KEY_IMAGE, R.drawable.ic_launcher);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen, container, false);
        ButterKnife.bind(this, view);
        descriptionTextView.setText(description);
        labelTextView.setText(label);
        imageView.setImageResource(resourceId);
        return view;
    }
}
