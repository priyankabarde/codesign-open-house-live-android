package com.jarvis.openhouselive.ui;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.jarvis.openhouselive.AppPreference;
import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.ui.custom.CustomButton;
import com.jarvis.openhouselive.ui.fragment.ScreenFragment;
import com.jarvis.openhouselive.util.Constants;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ArrayList<Fragment> fragments;

    @Bind(R.id.splash_btn)
    CustomButton mSplashBtn;
    @Bind(R.id.view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        if (AppPreference.getInstance().isWalkThroughDone()) {
            launchMain();
        } else {
            setScreens();
            ScreenSlidePagerAdapter adapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), fragments);
            viewPager.addOnPageChangeListener(this);
            viewPager.setAdapter(adapter);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //startActivity(new Intent(SplashActivity.this, MainActivity.class));
                //finish();
            }
        }, 2000);
    }

    private Bundle getBundle(int resourceId, String label, String description) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.EXTRA_KEY_IMAGE, resourceId);
        bundle.putString(Constants.EXTRA_KEY_DESCRIPTION, description);
        bundle.putString(Constants.EXTRA_KEY_LABEL, label);
        return bundle;
    }

    private void setScreens() {
        fragments = new ArrayList<>();
        fragments.add(ScreenFragment.newInstance(getBundle(R.drawable.c1, "Now get best of deals", "")));
        fragments.add(ScreenFragment.newInstance(getBundle(R.drawable.c2, "And Best of class service", "")));
        fragments.add(ScreenFragment.newInstance(getBundle(R.drawable.c3, "Right at your finger tips!", "")));
    }

    private void launchMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @OnClick({R.id.splash_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.splash_btn : {
                AppPreference.getInstance().setWalkThroughDone(true);
                launchMain();
                break;
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == fragments.size() - 1) {
            mSplashBtn.setVisibility(View.VISIBLE);
        } else {
            mSplashBtn.setVisibility(View.INVISIBLE);
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private ArrayList<Fragment> fragments;

        public ScreenSlidePagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
