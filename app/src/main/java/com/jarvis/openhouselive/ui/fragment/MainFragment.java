package com.jarvis.openhouselive.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import com.jarvis.openhouselive.AppManager;
import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.model.common.Listing;
import com.jarvis.openhouselive.model.response.ListingResponse;
import com.jarvis.openhouselive.rest.ApiManager;
import com.jarvis.openhouselive.rest.Requester;
import com.jarvis.openhouselive.ui.DetailActivity;
import com.jarvis.openhouselive.ui.MainActivity;
import com.jarvis.openhouselive.ui.PSActivity;
import com.jarvis.openhouselive.ui.PanormicActivity;
import com.jarvis.openhouselive.ui.adapter.ListingsAdapter;
import com.jarvis.openhouselive.util.Constants;
import com.jarvis.openhouselive.util.ToastUtil;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class MainFragment extends Fragment implements AdapterView.OnItemClickListener, ListingsAdapter.ListingCallback {

    private String locality;
    private ListingsAdapter listingsAdapter;

    @Bind(R.id.layout_progress_bar)
    LinearLayout progressBarLayout;
    @Bind(R.id.autocomplete_locality)
    AutoCompleteTextView autoCompleteTextView;
    @Bind(R.id.layout_main_recycler_view)
    RecyclerView recyclerView;

    public static MainFragment newInstance(Bundle bundle) {
        MainFragment fragment = new MainFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String getMyTag() {
        return Constants.TAG_FRAGMENT_MAIN;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.localities));
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setOnItemClickListener(this);

        listingsAdapter = new ListingsAdapter(getActivity(), this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listingsAdapter);

        locality = "";
        getListings();
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        locality = autoCompleteTextView.getText().toString();
        getListings();
    }

    private void getListings() {
        recyclerView.setVisibility(View.GONE);
        progressBarLayout.setVisibility(View.VISIBLE);
        ApiManager.getInstance().getListings(new WeakReference<>(listingRequester), "ban", locality, "", "", "");
    }

    private Requester listingRequester = new Requester() {
        @Override
        public void failure() {
            if (!isResumed()) {
                return;
            }

            progressBarLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            ToastUtil.showToastLong("Sorry could not fetch listings");
        }

        @Override
        public void success(Object response) {
            if (!isResumed()) {
                return;
            }

            try {
                ListingResponse listingResponse = (ListingResponse) response;
                listingsAdapter.clear();
                listingsAdapter.appendItems(listingResponse.getListings());
                progressBarLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                Log.e("TAG", e.getMessage());
            }
        }
    };



    @Override
    public void onCardboardRequested(Listing listing) {
        Intent intent = new Intent(getActivity(), PSActivity.class);
        intent.putExtra(Constants.EXTRA_KEY_LISTING, AppManager.getJsonString(listing));
        startActivity(intent);
    }

    @Override
    public void onDetailsRequested(Listing listing) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EXTRA_KEY_LISTING, AppManager.getJsonString(listing));
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onPanoramaRequested(Listing listing) {
        Intent intent = new Intent(getActivity(), PanormicActivity.class);
        intent.putExtra(Constants.EXTRA_KEY_LISTING, AppManager.getJsonString(listing));
        startActivity(intent);
    }

    @Override
    public void onShareRequested(Listing listing) {
        String string = new StringBuilder().
                append("Apartment : " + listing.getName()).append("\n").
                append("Rooms : " + listing.getType()).append("\n").
                append("Price : " + "\u20B9" + " " + listing.getPrice()).append("\n").
                append("Address : " + listing.getAddress()).toString();
        share("Share Listing", string);
    }

    private void share(String chooserText, String body) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, body);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, chooserText));
    }
}
